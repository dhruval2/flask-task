from app import ma
from .models import Competition
from User.utils import UserSerializer

class CompetitionSerializer(ma.Schema):

    class Meta:
        fields = ('id', 'title', 'social_issue', 'user_id',)    
        model = Competition   
