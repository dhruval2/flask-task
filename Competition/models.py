import uuid
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.dialects.postgresql import ARRAY
from sqlalchemy.orm import backref, relationship
from database import db
from User.models import CommonModel, User
from sqlalchemy import Column, ForeignKey, Integer, String, Numeric, DateTime, ForeignKey, CHAR, Table

# class User


class Competition(CommonModel):
    __tablename__ = 'competitions'

    id = db.Column(UUID(as_uuid=True),
                   primary_key=True, default=uuid.uuid4)
    title = db.Column(db.String())
    social_issue = db.Column(ARRAY(db.String()))
    user_id = db.Column(UUID, db.ForeignKey('users.id'), nullable=True)


    def __repr__(self):
        return f"<Competition {self.title}>"
