from flask import Blueprint, request, jsonify
from flask_restful import Resource, Api
from database import db

from .models import Competition
from .utils import CompetitionSerializer


competition_blueprint = Blueprint('competition', __name__)

api = Api(competition_blueprint)


class CompetitionApi(Resource):

    serializer_class = CompetitionSerializer()

    def post(self, *args, **kwargs):

        data = Competition(title= request.json.get('title',None), social_issue= request.json.get('social_issue',None), user_id= request.json.get('user_id',None))
        db.session.add(data)
        db.session.commit()

        return jsonify({'mess':'data added'})
    

    def get(self, *args, **kwargs):
        obj = Competition.query.all()
        data = self.serializer_class.dump(obj, many=True)
        return jsonify(data)




api.add_resource(CompetitionApi, '/competition')