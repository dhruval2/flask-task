from flask import Blueprint, request, jsonify
from flask_restful import Resource, Api
from database import db

from .models import Entry
from .utils import EntrySerializer


entry_blueprint = Blueprint('main', __name__)

api = Api(entry_blueprint)



class EntryApi(Resource):

    serializer_class = EntrySerializer()

    def get(self, *args, **kwargs):
        id = kwargs.pop('id', None)

        if id is not None:
            obj = Entry.query.filter_by(user_id=id)
            data = self.serializer_class.dump(obj, many=True)
            return jsonify(data)
        else:
            obj = Entry.query.all()
            data = self.serializer_class.dump(obj, many=True)
            return jsonify(data)

    def post(self, *args, **kwargs):
        data = Entry(**request.json)
        db.session.add(data)
        db.session.commit()
        return jsonify({'status': 'post'})




api.add_resource(EntryApi, '/entry', '/user-entry/<id>')