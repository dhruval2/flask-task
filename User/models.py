import uuid
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import backref
from sqlalchemy.orm import relationship


from database import db


class CommonModel(db.Model):
    __abstract__ = True

    created_on = db.Column(db.DateTime, default=db.func.now())
    updated_on = db.Column(
        db.DateTime, default=db.func.now(), onupdate=db.func.now())
    is_delete = db.Column(db.Boolean, default=False)



class User(CommonModel):
    __tablename__ = 'users'

    id = db.Column(UUID(as_uuid=True),
                   primary_key=True, default=uuid.uuid4)
    name = db.Column(db.String())
    email = db.Column(db.String())
    gender = db.Column(db.String())
    phone_number = db.Column(db.String())
    is_active = db.Column(db.Boolean, default=True)


    def __repr__(self):
        return f"<User {self.name}>"
