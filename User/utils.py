from app import ma
from User.models import User

class UserSerializer(ma.Schema):
    class Meta:
        model = User
        fields = ('id', 'name', 'email', 'gender', 'phone_number')
    
